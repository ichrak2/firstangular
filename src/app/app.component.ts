import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  books = [
    {title : 'FormationAngular 1',
    isDisabled : true},
    {title : 'FormationAngular 2',
    isDisabled : true},{title : 'FormationAngular 3',
    isDisabled : true}
  ]

}
