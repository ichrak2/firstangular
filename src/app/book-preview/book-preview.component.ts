import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-book-preview',
  templateUrl: './book-preview.component.html',
  styleUrls: ['./book-preview.component.css']
})
export class BookPreviewComponent implements OnInit {
  @Input() bookName:any;
  @Output() rate = new EventEmitter<any>();
  book :any;
  constructor() { }

  ngOnInit(): void {
  }

  ratingBook() {
    this.rate.emit(this.book)
  }

}
