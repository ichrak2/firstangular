import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {
  @Input() title = '';
  @Input() isDisabled = false;
  ratingBook: any;
  constructor() { }

  ngOnInit(): void {
  }

  books = [{
    bookName: 'Les Demons',
    bookAuthor: 'Fiodor Dostoïevski',
    country: "Russie",
    rating:0
  },
  {
    bookName: 'L\'Amour aux temps du choléra',
    bookAuthor: 'Gabriel García Márquez',
    country: "Colombie",
    rating:0
  },
  {
    bookName: 'L\'Étranger',
    bookAuthor: 'Albert',
    country: "Franc",
    rating:0
  }
]

  setDisabled(){
    this.isDisabled=!this.isDisabled;
  }

  onRate(event:any){
    this.books.map(book =>{
      if(book.rating === event.rating){
        book.rating=event.rating;
      }
    })
  }

}
