import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-film',
  templateUrl: './film.component.html',
  styleUrls: ['./film.component.css']
})
export class FilmComponent implements OnInit {
  count: number = 0;
  count2: number = 0;
  constructor() { }

  ngOnInit(): void {
  }
  films = [
    {
    filmName : 'The Hangover1',
    filmImg : ' assets/films/hangover1.jpg',
    likeNbr :   15    ,
    dislikeNbr : 3    ,
    available : true
    },
    {
      filmName : 'The Hangover2',
      filmImg : ' assets/films/hangover2.jpg',
      likeNbr : 15,
      dislikeNbr : 30,
      available : true
      },
      {
        filmName : 'The Hangover3',
        filmImg : ' assets/films/hangover3.jpg',
        likeNbr : 20,
        dislikeNbr : 20,
        available : true
        }
    ]

    like(any: { likeNbr: number; }){
      any.likeNbr++;
      console.log('j\'aime ce film');
    }

    dislike( any: { dislikeNbr: number; }){
      any.dislikeNbr++;
      console.log('je n\'aime ce film');
    }

}
